﻿using System;
using System.Collections.Generic;
using System.Text;
using NewLife.Xml;

namespace GitPuller.Configurations
{
    public class GitPullerConfiguration : XmlConfig<GitPullerConfiguration>
    {
        public GitPullerConfiguration()
        {
            this.Root = "/volume1/backup/src";
        }
        public String Root { get; set; }
    }
}
