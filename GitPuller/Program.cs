﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using GitPuller.Configurations;
using NewLife.Log;

namespace GitPuller
{
    internal class Program
    {
        private static async Task GitPull(string dir)
        {
            var info = new ProcessStartInfo("git")
            {
                CreateNoWindow = true,
                Arguments = "pull",
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                WorkingDirectory = dir
            };
            using (var p = new Process { StartInfo = info })
            {
                p.Start();
                var output = p.StandardOutput;
                while (!output.EndOfStream)
                {
                    var line = await output.ReadLineAsync();
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        XTrace.WriteLine(line);
                    }
                }

                p.WaitForExit();
            }
        }

        private static void SvnUpdate(string dir)
        {
            var info = new ProcessStartInfo("svn")
            {
                CreateNoWindow = true,
                Arguments = "update",
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                WorkingDirectory = dir
            };
            info.Environment.Add("LC_ALL", "en_US.UTF-8");
            info.Environment.Add("LANG", "en_US.UTF-8");
            info.Environment.Add("LANGUAGE", "en_US.UTF-8");

            using (var p = new Process { StartInfo = info })
            {
                p.OutputDataReceived += (s, e) =>
                {
                    if (string.IsNullOrWhiteSpace(e.Data))
                    {
                        return;
                    }

                    XTrace.WriteLine(e.Data);
                };
                p.ErrorDataReceived += (s, e) =>
                {
                    if (string.IsNullOrWhiteSpace(e.Data))
                    {
                        return;
                    }

                    XTrace.WriteLine(e.Data);
                };
                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
                p.WaitForExit();
            }
        }

        private static async Task Main(string[] args)
        {
            XTrace.UseConsole();
            var cfg = GitPullerConfiguration.Current;
            var root = cfg.Root;
            XTrace.WriteLine("Root:{0}", root);
            var queue = new Queue<string>();
            queue.Enqueue(root);
            var times = 0;
            while (queue.Count > 0)
            {
                var dir = queue.Dequeue();
                times++;
                if (times % 50 == 0)
                {
                    XTrace.WriteLine("Scan: {0} at :{1}", times, dir);
                }

                var children = Directory.GetDirectories(dir);
                var isGit = false;
                var isSvn = false;
                foreach (var child in children)
                {
                    var name = Path.GetFileName(child);
                    if (name == ".git" && File.Exists(Path.Combine(child, "index")))
                    {
                        isGit = true;
                        break;
                    }

                    if (name == ".svn" && File.Exists(Path.Combine(child, "format")))
                    {
                        isSvn = true;
                        break;
                    }
                }

                if (isGit)
                {
                    XTrace.WriteLine("{0} is git directory", dir);
                    await GitPull(dir);
                }
                else if (isSvn)
                {
                    XTrace.WriteLine("{0} is svn directory", dir);
                    SvnUpdate(dir);
                }
                else
                {
                    foreach (var child in children)
                    {
                        queue.Enqueue(child);
                    }
                }
            }

            XTrace.WriteLine("Done!");
        }
    }
}